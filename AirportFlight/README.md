# Test 2 :

Create a tiny web application ASP.NET (MVC) with bootstrap theme (or other) :

    Display a Flight list
        a single flight consists of a name, a departure airport and an arrival airport
        the list will also display the computed distance between the 2 airports (each airport has some GPS coordinates)
    The user can add / delete / modify a flight from the list

The developer is evaluated on the architecture of the application (not a single project! But separate projects CSPROJ in the VS Solution)
Proper code (injection, interface, design pattern, etc�) and unit tests are expected
No constraint is done for the storage (it can be a SQL Database, a flat file, etc�) the developer can decide freely.
As the airport list does not need to be input by user, it can be stored / injected directly in the backend storage chosen by the developer.