﻿namespace AirportFlight.Application.Interfaces
{
    /// <summary>
    /// A repository interface for the Clean Architecture.
    /// </summary>
    /// <typeparam name="T">A class model</typeparam>
    public interface IRepository<T> where T : class
    {
        Task<IReadOnlyList<T>> GetAllAsync();
        Task<T> GetByIdAsync(long id);
        Task<string> AddAsync(T entity);
        Task<string> UpdateAsync(T entity);
        Task<string> DeleteAsync(long id);
    }
}