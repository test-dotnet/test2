﻿using AirportFlight.Core.Entities;

namespace AirportFlight.Application.Interfaces
{
    /// <summary>
    /// A repository interface for Flight class.
    /// </summary>
    public interface IFlightRepository : IRepository<Flight>
    {
    }
}
