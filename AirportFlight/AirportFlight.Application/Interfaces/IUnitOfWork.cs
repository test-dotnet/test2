﻿namespace AirportFlight.Application.Interfaces
{
    public interface IUnitOfWork
    {
        IFlightRepository Flights { get; }
    }
}
