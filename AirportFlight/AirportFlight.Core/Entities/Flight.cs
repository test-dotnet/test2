﻿using System.ComponentModel.DataAnnotations;

namespace AirportFlight.Core.Entities
{
    /// <summary>
    /// Flight class representing the model on Database.
    /// </summary>
    public class Flight
    {
        [Key]
        public int FlightId { get; set; }
        [Required]
        [Display(Name = "Flight Details")]
        public string Name { get; set; }
        public string? DepartLocation { get; set; }
        public string? DestinationLocation { get; set; }
    }
}