﻿using AirportFlight.Api.Models;
using AirportFlight.Application.Interfaces;
using AirportFlight.Core.Entities;
using AirportFlight.Logging;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;

namespace AirportFlight.Api.Controllers
{
    /// <summary>
    /// Controller used to expose Flight Api CRUD management.
    /// </summary>
    public class FlightsController : BaseApiController
    {
        #region ===[ Private Members ]=============================================================

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region ===[ Constructor ]=================================================================
        /// <summary>
        /// Initialize FlightController by injecting an object type of IUnitOfWork
        /// </summary>
        public FlightsController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        #endregion

        #region ===[ Public Methods ]==============================================================
        /// <summary>
        /// Get All list of Flight.
        /// </summary>
        /// <returns>List<Flight></returns>
        [HttpGet]
        public async Task<ApiResponse<List<Flight>>> GetAll()
        {
            var apiResponse = new ApiResponse<List<Flight>>();

            try
            {
                var data = await _unitOfWork.Flights.GetAllAsync();
                apiResponse.Success = true;
                apiResponse.Result = data.ToList();
            }
            catch (SqlException ex)
            {
                apiResponse.Success = false;
                apiResponse.Message = ex.Message;
                Logger.Instance.Error("SQL Exception:", ex);
            }
            catch (Exception ex)
            {
                apiResponse.Success = false;
                apiResponse.Message = ex.Message;
                Logger.Instance.Error("Exception:", ex);
            }

            return apiResponse;
        }

        /// <summary>
        /// Get A Flight by Id.
        /// </summary>
        /// <param name="id">Flight ID</param>
        /// <returns>Flight</returns>
        [HttpGet("{id}")]
        public async Task<ApiResponse<Flight>> GetById(int id)
        {

            var apiResponse = new ApiResponse<Flight>();

            try
            {
                var data = await _unitOfWork.Flights.GetByIdAsync(id);
                apiResponse.Success = true;
                apiResponse.Result = data;
            }
            catch (SqlException ex)
            {
                apiResponse.Success = false;
                apiResponse.Message = ex.Message;
                Logger.Instance.Error("SQL Exception:", ex);
            }
            catch (Exception ex)
            {
                apiResponse.Success = false;
                apiResponse.Message = ex.Message;
                Logger.Instance.Error("Exception:", ex);
            }

            return apiResponse;
        }

        /// <summary>
        /// Create a Flight.
        /// </summary>
        /// <param name="flight"></param>
        /// <returns>ApiResponse<string></returns>
        [HttpPost]
        public async Task<ApiResponse<string>> Add(Flight flight)
        {
            var apiResponse = new ApiResponse<string>();

            try
            {
                var data = await _unitOfWork.Flights.AddAsync(flight);
                apiResponse.Success = true;
                apiResponse.Result = data;
            }
            catch (SqlException ex)
            {
                apiResponse.Success = false;
                apiResponse.Message = ex.Message;
                Logger.Instance.Error("SQL Exception:", ex);
            }
            catch (Exception ex)
            {
                apiResponse.Success = false;
                apiResponse.Message = ex.Message;
                Logger.Instance.Error("Exception:", ex);
            }

            return apiResponse;
        }

        /// <summary>
        /// Update a Flight.
        /// </summary>
        /// <param name="flight">Flight object</param>
        /// <returns>ApiResponse<string></returns>
        [HttpPut]
        public async Task<ApiResponse<string>> Update(Flight flight)
        {
            var apiResponse = new ApiResponse<string>();

            try
            {
                var data = await _unitOfWork.Flights.UpdateAsync(flight);
                apiResponse.Success = true;
                apiResponse.Result = data;
            }
            catch (SqlException ex)
            {
                apiResponse.Success = false;
                apiResponse.Message = ex.Message;
                Logger.Instance.Error("SQL Exception:", ex);
            }
            catch (Exception ex)
            {
                apiResponse.Success = false;
                apiResponse.Message = ex.Message;
                Logger.Instance.Error("Exception:", ex);
            }

            return apiResponse;
        }

        /// <summary>
        /// Delete a Flight.
        /// </summary>
        /// <param name="id">Flight ID</param>
        /// <returns>ApiResponse<string></returns>
        [HttpDelete]
        public async Task<ApiResponse<string>> Delete(int id)
        {
            var apiResponse = new ApiResponse<string>();

            try
            {
                var data = await _unitOfWork.Flights.DeleteAsync(id);
                apiResponse.Success = true;
                apiResponse.Result = data;
            }
            catch (SqlException ex)
            {
                apiResponse.Success = false;
                apiResponse.Message = ex.Message;
                Logger.Instance.Error("SQL Exception:", ex);
            }
            catch (Exception ex)
            {
                apiResponse.Success = false;
                apiResponse.Message = ex.Message;
                Logger.Instance.Error("Exception:", ex);
            }

            return apiResponse;
        }

        #endregion
    }
}
