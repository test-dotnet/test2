﻿using AirportFlight.Api.Filter;
using Microsoft.AspNetCore.Mvc;

namespace AirportFlight.Api.Controllers
{
    /// <summary>
    /// A Modelbase of Controller who will be used by all Controllers in the Api project.
    /// </summary>
    [Route("api/[controller]")]
    [TypeFilter(typeof(AuthorizationFilterAttribute))]
    [ApiController]
    public class BaseApiController : ControllerBase
    {
    }
}
