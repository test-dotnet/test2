﻿using Microsoft.AspNetCore.Mvc;

namespace AirportFlight.Api.Controllers
{
    /// <summary>
    /// Auth Controller who will be used for security controle in the Api data management project.
    /// </summary>
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class AuthController : Controller
    {
        [HttpGet]
        public IActionResult NotAuthorized()
        {
            return Unauthorized();
        }
    }
}
