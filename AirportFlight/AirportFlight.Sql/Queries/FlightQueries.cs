﻿using System.Diagnostics.CodeAnalysis;

namespace AirportFlight.Sql.Queries
{
    /// <summary>
    /// List of Query request in Flight Table.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public static class FlightQueries
    {
        // Get all Flight.
        public static string AllFlight => "SELECT * FROM [Flight] (NOLOCK)";

        // Get a specific Flight
        public static string FlightById => "SELECT * FROM [Flight] (NOLOCK) WHERE [FlightId] = @FlightId";

        // Create a Flight
        public static string AddFlight =>
            @"INSERT INTO [Flight] ([Name], [DepartLocation], [DestinationLocation]) 
            VALUES (@Name, @DepartLocation, @DestinationLocation)";

        // Update a Flight
        public static string UpdateFlight =>
            @"UPDATE [Flight] 
            SET [Name] = @Name, 
				[DepartLocation] = @DepartLocation, 
				[DestinationLocation] = @DestinationLocation
            WHERE [FlightId] = @FlightId";

        // Delete a Flight
        public static string DeleteFlight => "DELETE FROM [Flight] WHERE [FlightId] = @FlightId";
    }
}