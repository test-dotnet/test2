using AirportFlight.Api.Controllers;
using AirportFlight.Application.Interfaces;
using AirportFlight.Core.Entities;
using AirportFlight.Infrastructure.Repository;
using AirportFlight.Test.Helper;
using Microsoft.Extensions.Configuration;
using Moq;

namespace AirportFlight.Test.IntegrationTests
{
    [TestClass]
    public class FlightControllerShould
    {
        #region ===[ Private Members ]=============================================================

        protected readonly IConfigurationRoot _configuration;
        private readonly FlightsController _controllerObj;
        private readonly FlightsController _moqControllerObj;
        private readonly Mock<IUnitOfWork> _moqRepo;

        #endregion

        #region ===[ Constructor ]=================================================================

        /// <summary>
        /// Constructor
        /// </summary>
        public FlightControllerShould()
        {
            _configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                                                      .AddJsonFile("appsettings.json")
                                                      .Build();

            var repository = new FlightRepository(_configuration);
            var unitofWork = new UnitOfWork(repository);

            _controllerObj = new FlightsController(unitofWork);

            _moqRepo = new Mock<IUnitOfWork>();
            _moqControllerObj = new FlightsController(_moqRepo.Object);
        }

        #endregion

        #region ===[ Test Methods ]================================================================

        [TestMethod]
        public async Task AddUpdateDeleteAndGetFlight()
        {
            //Add Flight
            await SaveFlight();

            //Get All Flight
            var flight = await GetAll();

            //Update Flight
            await UpdateFlight(flight);

            var flightId = flight.FlightId;
            //Get Flight By Id
            await GetById(flightId);

            //Delete Flight
            await DeleteFlight(flightId);
        }

        [TestMethod]
        public async Task GetAll_Throw_Exception()
        {
            //SQL Exception Test.
            _moqRepo.Setup(x => x.Flights.GetAllAsync()).Throws(TestConstants.GetSqlException());

            var result = await _moqControllerObj.GetAll();

            Assert.IsFalse(result.Success);
            Assert.IsNull(result.Result);

            //General Exception Test.
            _moqRepo.Setup(x => x.Flights.GetAllAsync()).Throws(TestConstants.GetGeneralException());

            result = await _moqControllerObj.GetAll();

            Assert.IsFalse(result.Success);
            Assert.IsNull(result.Result);
            System.Diagnostics.Trace.WriteLine("Get List All Flight And Throw Exception if Error");
        }

        [TestMethod]
        public async Task GetById_Throw_Exception()
        {
            //SQL Exception Test.
            _moqRepo.Setup(x => x.Flights.GetByIdAsync(It.IsAny<long>())).Throws(TestConstants.GetSqlException());

            var result = await _moqControllerObj.GetById(1);

            Assert.IsFalse(result.Success);
            Assert.IsNull(result.Result);

            //General Exception Test.
            _moqRepo.Setup(x => x.Flights.GetByIdAsync(It.IsAny<long>())).Throws(TestConstants.GetGeneralException());

            result = await _moqControllerObj.GetById(1);

            Assert.IsFalse(result.Success);
            Assert.IsNull(result.Result);
            System.Diagnostics.Trace.WriteLine("Get Flight By Id And Throw Exception if Error");
        }

        [TestMethod]
        public async Task Add_Throw_Exception()
        {
            //SQL Exception Test.
            _moqRepo.Setup(x => x.Flights.AddAsync(It.IsAny<Flight>())).Throws(TestConstants.GetSqlException());

            var result = await _moqControllerObj.Add(new Flight());

            Assert.IsFalse(result.Success);

            //General Exception Test.
            _moqRepo.Setup(x => x.Flights.AddAsync(It.IsAny<Flight>())).Throws(TestConstants.GetGeneralException());

            result = await _moqControllerObj.Add(new Flight());

            Assert.IsFalse(result.Success);
            System.Diagnostics.Trace.WriteLine("Create Flight And Throw Exception if Error");
        }

        [TestMethod]
        public async Task Update_Throw_Exception()
        {
            //SQL Exception Test.
            _moqRepo.Setup(x => x.Flights.UpdateAsync(It.IsAny<Flight>())).Throws(TestConstants.GetSqlException());

            var result = await _moqControllerObj.Update(new Flight());

            Assert.IsFalse(result.Success);

            //General Exception Test.
            _moqRepo.Setup(x => x.Flights.UpdateAsync(It.IsAny<Flight>())).Throws(TestConstants.GetGeneralException());

            result = await _moqControllerObj.Update(new Flight());

            Assert.IsFalse(result.Success);
            System.Diagnostics.Trace.WriteLine("Update Flight And Throw Exception if Error");
        }

        [TestMethod]
        public async Task Delete_Throw_Exception()
        {
            //SQL Exception Test.
            _moqRepo.Setup(x => x.Flights.DeleteAsync(It.IsAny<long>())).Throws(TestConstants.GetSqlException());

            var result = await _moqControllerObj.Delete(1);

            Assert.IsFalse(result.Success);

            //General Exception Test.
            _moqRepo.Setup(x => x.Flights.DeleteAsync(It.IsAny<long>())).Throws(TestConstants.GetGeneralException());

            result = await _moqControllerObj.Delete(1);

            Assert.IsFalse(result.Success);
            System.Diagnostics.Trace.WriteLine("Delete Flight And Throw Exception if Error");
        }

        #endregion

        #region ===[ Private Methods ]=============================================================

        private async Task SaveFlight()
        {
            var flight = new Flight
            {
                Name = TestConstants.FlightTest.Name,
                DepartLocation = TestConstants.FlightTest.DepartLocation,
                DestinationLocation = TestConstants.FlightTest.DestinationLocation
            };

            var result = await _controllerObj.Add(flight);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Success);
            System.Diagnostics.Trace.WriteLine("Validate Save Flight");
        }

        private async Task UpdateFlight(Flight flight)
        {
            //Update Name Address.
            flight.Name = TestConstants.FlightTest.Name;
            var result = await _controllerObj.Update(flight);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Success);
            System.Diagnostics.Trace.WriteLine("Validate Update Flight");
        }

        private async Task<Flight> GetAll()
        {
            var result = await _controllerObj.GetAll();

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Success);
            Assert.IsTrue(result.Result.Count > 0);
            System.Diagnostics.Trace.WriteLine("Get All List of Flight");
            var flight = result.Result
                            .Where(x => x.Name == TestConstants.FlightTest.Name
                                    && x.DepartLocation == TestConstants.FlightTest.DepartLocation
                                    && x.DestinationLocation == TestConstants.FlightTest.DestinationLocation).First();

            return flight;
        }

        private async Task GetById(int flightId)
        {
            var result = await _controllerObj.GetById(flightId);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Success);
            Assert.IsNotNull(result.Result);
            System.Diagnostics.Trace.WriteLine("Get Flight By Id");
        }

        private async Task DeleteFlight(int flightId)
        {
            var result = await _controllerObj.Delete(flightId);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Success);
            System.Diagnostics.Trace.WriteLine("Delete Flight By Id");
        }

        #endregion
    }
}