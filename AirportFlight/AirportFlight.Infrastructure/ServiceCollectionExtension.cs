﻿using AirportFlight.Application.Interfaces;
using AirportFlight.Infrastructure.Repository;
using Microsoft.Extensions.DependencyInjection;

namespace AirportFlight.Infrastructure
{
    /// <summary>
    /// Class to register all Interface implementation.
    /// </summary>
    public static class ServiceCollectionExtension
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddTransient<IFlightRepository, FlightRepository>();
            services.AddTransient<IUnitOfWork, UnitOfWork>();
        }
    }
}
