﻿using AirportFlight.Application.Interfaces;

namespace AirportFlight.Infrastructure.Repository
{
    /// <summary>
    /// UnitOfWork class implementing IUnitOfWork Interface.
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(IFlightRepository flightRepository)
        {
            Flights = flightRepository;
        }

        public IFlightRepository Flights { get; set; }
    }
}
