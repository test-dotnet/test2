﻿using AirportFlight.Application.Interfaces;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data;
using AirportFlight.Core.Entities;
using Dapper;
using AirportFlight.Sql.Queries;

namespace AirportFlight.Infrastructure.Repository
{
    /// <summary>
    /// FlightRepository class implementing IFlightRepository Interface.
    /// </summary>
    public class FlightRepository : IFlightRepository
    {
        #region ===[ Private Members ]=============================================================

        private readonly IConfiguration configuration;

        #endregion

        #region ===[ Constructor ]=================================================================

        public FlightRepository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        #endregion

        #region ===[ IFlightRepository Methods ]==================================================

        public async Task<IReadOnlyList<Flight>> GetAllAsync()
        {
            using (IDbConnection connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.QueryAsync<Flight>(FlightQueries.AllFlight);
                return result.ToList();
            }
        }

        public async Task<Flight> GetByIdAsync(long id)
        {
            using (IDbConnection connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.QuerySingleOrDefaultAsync<Flight>(FlightQueries.FlightById, new { FlightId = id });
                return result;
            }
        }

        public async Task<string> AddAsync(Flight entity)
        {
            using (IDbConnection connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.ExecuteAsync(FlightQueries.AddFlight, entity);
                return result.ToString();
            }
        }

        public async Task<string> UpdateAsync(Flight entity)
        {
            using (IDbConnection connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.ExecuteAsync(FlightQueries.UpdateFlight, entity);
                return result.ToString();
            }
        }

        public async Task<string> DeleteAsync(long id)
        {
            using (IDbConnection connection = new SqlConnection(configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();
                var result = await connection.ExecuteAsync(FlightQueries.DeleteFlight, new { FlightId = id });
                return result.ToString();
            }
        }

        #endregion
    }
}