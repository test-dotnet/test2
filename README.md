# AirportFlight Project

## About the project

Project based from the second subject in the .NET Test.

Create a tiny web application ASP.NET (MVC) with bootstrap theme (or other) :

    Display a Flight list
        a single flight consists of a name, a departure airport and an arrival airport
        the list will also display the computed distance between the 2 airports (each airport has some GPS coordinates)
    The user can add / delete / modify a flight from the list

The developer is evaluated on the architecture of the application (not a single project! But separate projects CSPROJ in the VS Solution)
Proper code (injection, interface, design pattern, etc…) and unit tests are expected
No constraint is done for the storage (it can be a SQL Database, a flat file, etc…) the developer can decide freely.
As the airport list does not need to be input by user, it can be stored / injected directly in the backend storage chosen by the developer.

### Built With
![.NET][dotnet]
![.NET][ASP NET Core Mvc]
![.NET][ASP NET Web API]
 

### Tools
- Visual Studio 2022 and .NET 7.0
- SQL Server 2022 & MSSQL Studio 19
- Visual Studio Code 1.8x
- Git


### Features
* Back-end Web API (**AirportFlight**) based from these Stack:
- Clean Architecture & Repository Pattern
- Swagger UI
- Dapper (mini ORM)
- API Authentication (Key Based)
- Unit Testing (MSTest Project)

* Front-end ASP.NET Core MVC (**AirportFlight.MvcUI**) based from these Stack:
- ASP.NET Core Mvc template


## Prerequisites
* .net 7.0 LTS ( SDK & Runtime)
* Windows ou Linux based system


## Installation
1. Clone or download the repository
```sh
git clone git@gitlab.com:test-dotnet/test2.git
cd test2
```
2. Create your database with the script **Flight.sql** located:

```sh
cd AirportFlight/AirportFlight.Sql/Scripts/
```

3. Install front-end and next back-end dependencies
```sh
cd [AirportFlight or AirportFlight.MvcUI]
dotnet restore
dotnet build
dotnet run
```
4. Run back-end (from back folder) **first** (***endpoint: https://localhost:7204/Swagger/index.html***)
```sh
cd [AirportFlight]
dotnet build
dotnet run
```
Or run it on Microsoft Visual Studio

5. Start the UI application (from client folder) **second** (***optional if you want to experiment UI interface***)
```sh
cd [AirportFlight.MvcUI]
dotnet build
dotnet run
```
Or run it on Microsoft Visual Studio


### Web API  Security Key
- 04577BA6-3E32-456C-B528-E41E20D28D79
or
- 6D5D1ABA-4F78-4DD3-A69D-C2D15F2E259A,709C95E7-F59D-4CC4-9638-4CDE30B2FCFD
