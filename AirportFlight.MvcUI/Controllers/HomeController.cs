﻿using AirportFlight.MvcUI.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Net.Http.Headers;
using System.Text;

namespace AirportFlight.MvcUI.Controllers
{
    public class HomeController : Controller
    {
        #region APi Configuration
        private static HttpClient _httpClient = new()
        {
            BaseAddress = new Uri("https://localhost:7204/api/Flights"),
        };

        private readonly ILogger<HomeController> _logger;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="logger"></param>
        public HomeController(ILogger<HomeController> logger)
        {
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _httpClient.DefaultRequestHeaders.Add("Authorization", "04577BA6-3E32-456C-B528-E41E20D28D79");
            _logger = logger;
        }

        #endregion

        #region List Flight

        /// <summary>
        /// Display All Flight List.
        /// </summary>
        /// <returns>List<Flight></returns>
        public async Task<IActionResult> IndexAsync()
        {
            List<Flight> flightList = new List<Flight>();
            using (var response = await _httpClient.GetAsync(""))
            {
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    ApiResponse<Flight[]?>? apiContent = JsonConvert.DeserializeObject<ApiResponse<Flight[]?>>(apiResponse);
                    bool? status = apiContent.Success; 
                    if((bool)status)
                        flightList = apiContent.Result.ToList<Flight>();
                }
                else
                    ViewBag.StatusCode = response.StatusCode;
            }
            return View(flightList);
        }

        public ViewResult GetFlight() => View();

        /// <summary>
        /// Display Specific Flight element.
        /// </summary>
        /// <param name="id">Flight ID</param>
        /// <returns>Flight?</returns>
        [HttpPost]
        public async Task<IActionResult> GetFlight(int flightId)
        {
            Flight? flight = new Flight();
            using (var response = await _httpClient.GetAsync("?id=" + flightId.ToString()))
            {
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.OK) 
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        ApiResponse<Flight[]?>? apiContent = JsonConvert.DeserializeObject<ApiResponse<Flight[]?>>(apiResponse);
                        bool? status = apiContent.Success; 
                        if((bool)status)
                            flight = apiContent.Result.FirstOrDefault(x => x.FlightId == flightId);
                    }
                }
                else
                    ViewBag.StatusCode = response.StatusCode;
            }
            return View(flight);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        #endregion

        #region Create Flight

        /// <summary>
        /// Display Add Flight Form.
        /// </summary>
        /// <returns>View</returns>
        public ViewResult AddFlight() => View();

        /// <summary>
        /// Create A Flight.
        /// </summary>
        /// <param name="flight">Flight Data</param>
        /// <returns>View</returns>
        [HttpPost]
        public async Task<IActionResult> AddFlight(Flight flight)
        {
            StringContent content = new StringContent(JsonConvert.SerializeObject(flight), Encoding.UTF8, "application/json");
            using (var response = await _httpClient.PostAsync("", content))
            {
                if (response.StatusCode == System.Net.HttpStatusCode.OK) 
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();                    
                    ApiResponse<string?>? apiContent = JsonConvert.DeserializeObject<ApiResponse<string?>>(apiResponse);
                    bool? status = apiContent.Success; 
                    if((bool)status && apiContent.Result == "1")
                        return View(flight);
                }
                else
                    ViewBag.StatusCode = response.StatusCode;
            }
            return RedirectToAction("Index");
        }

        #endregion

        #region Update Flight

        /// <summary>
        /// Display View to Update a Flight.
        /// </summary>
        /// <param name="id">Flight ID</param>
        /// <returns>View(flight)</returns>
        public async Task<IActionResult> UpdateFlight(int id)
        {
            Flight? flight = new Flight();
            using (var response = await _httpClient.GetAsync("?id=" + id.ToString()))
            {
                if (response.StatusCode == System.Net.HttpStatusCode.OK) 
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    ApiResponse<Flight[]?>? apiContent = JsonConvert.DeserializeObject<ApiResponse<Flight[]?>>(apiResponse);
                    bool? status = apiContent.Success; 
                    if((bool)status)
                        flight = apiContent.Result.FirstOrDefault(x => x.FlightId == id);
                }
                else
                    ViewBag.StatusCode = response.StatusCode;
            }
            return View(flight);
        }

        /// <summary>
        /// Update a Flight Data.
        /// </summary>
        /// <param name="flight">Flight Data</param>
        /// <returns>View(flight)</returns>
        [HttpPost]
        public async Task<IActionResult> UpdateFlight(Flight flight)
        {
            StringContent content = new StringContent(JsonConvert.SerializeObject(flight), Encoding.UTF8, "application/json");   
            using (var response = await _httpClient.PutAsync("", content))
            {
                if (response.StatusCode == System.Net.HttpStatusCode.OK) 
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    ApiResponse<string?>? apiContent = JsonConvert.DeserializeObject<ApiResponse<string?>>(apiResponse);
                    bool? status = apiContent.Success; 
                    if((bool)status && apiContent.Result == "1")
                        ViewBag.Result = "Success";
                }
            }
            return View(flight);
        }
        
        #endregion
        
        #region Delete Flight

        /// <summary>
        /// Delete an existing Flight.
        /// </summary>
        /// <param name="flightId">Flight ID</param>
        /// <returns>Views</returns>
        [HttpPost]
        public async Task<IActionResult> DeleteFlight(int flightId)
        {
            using (var response = await _httpClient.DeleteAsync("?id=" + flightId.ToString()))
            {
                if (response.StatusCode == System.Net.HttpStatusCode.OK) 
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();                    
                    ApiResponse<string?>? apiContent = JsonConvert.DeserializeObject<ApiResponse<string?>>(apiResponse);
                    bool? status = apiContent.Success; 
                    if((bool)status && apiContent.Result != "1")
                        ViewBag.StatusCode = response.StatusCode;
                }
                else
                    ViewBag.StatusCode = response.StatusCode;
            }
            return RedirectToAction("Index");
        }

        #endregion
    }
}