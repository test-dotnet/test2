﻿namespace AirportFlight.MvcUI.Models
{
    /// <summary>
    /// Flight class representing the model on Database in the AirportFlight API project.
    /// </summary>
    public class Flight
    {
        public int FlightId { get; set; }
        public string Name { get; set; }
        public string? DepartLocation { get; set; }
        public string? DestinationLocation { get; set; }
    }

    /// <summary>
    /// API Response Data Model. 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ApiResponse<T>
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public T Result { get; set; }
    }
}
